;;;; state-machine.asd

(asdf:defsystem #:state-machine
  :description "Describe state-machine here"
  :author "Angelo Rossi <angelo.rossi.homelab@gmail.com>"
  :license "GPL3"
  :depends-on (#:bordeaux-threads)
  :serial t
  :components ((:file "package")
               (:file "state-machine")))

