;;;; state-machine.lisp

(in-package #:state-machine)

;;; "state-machine" goes here. Hacks and glory await!

;;
;; Class definition.
;;

(defclass state-machine-descriptor ()
  ((name
    :initarg :name
    :initform nil
    :accessor name)
   (lock-object
    :initarg :lock-object
    :initform nil
    :accessor lock-object)
   (condition-lock-object
    :initarg :condition-lock-object
    :initform nil
    :accessor condition-lock-object)   
   (condition-object
    :initarg :condition-object
    :initform nil
    :accessor condition-object)
   (thread-object
    :initarg :thread-object
    :initform nil
    :accessor thread-object)
   (current-state
    :initarg :current-state
    :initform nil
    :accessor current-state)
   (events-queue
    :initarg :events-queue
    :initform nil
    :accessor events-queue)
   (handlers-table
    :initarg :handlers-table
    :initform nil
    :accessor handlers-table)))

;;
;; Generic function definitions.
;;

(defgeneric start (object &rest parameters &key initial-state))
(defgeneric stop (object))
(defgeneric get-name (object))
(defgeneric set-name (object name))
(defgeneric get-lock (object))
(defgeneric set-lock (object lock))
(defgeneric get-condition-lock (object))
(defgeneric set-condition-lock (object lock))
(defgeneric get-condition (object))
(defgeneric set-condition (object condition))
(defgeneric get-thread (object))
(defgeneric set-thread (object thread))
(defgeneric get-current-state (object))
(defgeneric set-current-state (object state))
(defgeneric get-events-queue (object))
(defgeneric set-events-queue (object events-queue))
(defgeneric flush-events-queue (object))
(defgeneric running-p (object))
(defgeneric thread (object))
(defgeneric push-event (object event))
(defgeneric pop-event (object))

;;
;; Functions.
;;

(defun make-state-machine (&rest parameters &key (name (symbol-name (gensym "state-machine-"))) (initial-state :idle))
  (declare (ignorable parameters name))
  (locally (declare (special *object*)))
  (let ((object (make-instance 'state-machine-descriptor)))
    (setf (name object) name
	  (current-state object) initial-state
	  (handlers-table object) (make-hash-table)
	  (lock-object object) (bordeaux-threads:make-lock (symbol-name (gensym (concatenate 'string name "-lock-"))))
	  (condition-object object) (bordeaux-threads:make-condition-variable :name (symbol-name (gensym "state-machine-condition-")))
	  (condition-lock-object object) (bordeaux-threads:make-lock (symbol-name (gensym "state-machine-condition-lock-"))))
    object))


;;
;; Methods.
;;

(defmethod get-name ((object state-machine-descriptor))
  (name object))

(defmethod set-name ((object state-machine-descriptor) name)
  (bordeaux-threads:with-lock-held ((lock-object object))
    (setf (name object) name)))

(defmethod get-lock ((object state-machine-descriptor))
  (lock-object object))

(defmethod set-lock ((object state-machine-descriptor) lock)
  (bordeaux-threads:with-lock-held ((lock-object object))
    (setf (lock-object) lock)))

(defmethod get-condition-lock ((object state-machine-descriptor))
  (condition-lock-object object))

(defmethod set-condition-lock ((object state-machine-descriptor) lock)
  (bordeaux-threads:with-lock-held ((lock-object object))
    (setf (condition-lock-object) lock)))

(defmethod get-condition ((object state-machine-descriptor))
  (condition-object object))

(defmethod set-condition ((object state-machine-descriptor) condition)
  (bordeaux-threads:with-lock-held ((lock-object object))
    (setf (condition-object) condition)))

(defmethod get-thread ((object state-machine-descriptor))
  (thread-object object))

(defmethod set-thread ((object state-machine-descriptor) thread)
  (when (bordeaux-threads:threadp thread)
    (unless (bordeaux-threads:thread-alive-p (thread-object object))
      (bordeaux-threads:with-lock-held ((lock-object object))
	(setf (thread-object object) thread)))))

(defmethod get-current-state ((object state-machine-descriptor))
  (current-state object))

(defmethod set-current-state ((object state-machine-descriptor) new-state)
  (bordeaux-threads:with-lock-held ((lock-object object))
    (setf (current-state object) new-state)))

(defmethod get-states-count ((object state-machine-descriptor))
  (hash-table-count (handlers-table object)))

(defmethod get-events-queue ((object state-machine-descriptor))
  (events-queue object))

(defmethod set-events-queue ((object state-machine-descriptor) events-queue)
  (bordeaux-threads:with-lock-held ((events-queue object))
    (setf (events-queue object) events-queue)))

(defmethod flush-events-queue ((object state-machine-descriptor))
  (loop while (events-queue object) do
       (pop (events-queue object))))

(defmethod add-state-handler ((object state-machine-descriptor) &rest parameters &key state handler &allow-other-keys)
  (declare (ignorable parameters state handler))
  (multiple-value-bind (f status)
      (gethash state (handlers-table object))
    (unless status
      (when (functionp handler)
	(bordeaux-threads:with-lock-held ((lock-object object))    
	  (setf (gethash state (handlers-table object)) handler))))))

(defmethod running-p ((object state-machine-descriptor))
  (when (bordeaux-threads:threadp (thread-object object))
    (bordeaux-threads:thread-alive-p (thread-object object))))

(defmethod start ((object state-machine-descriptor) &rest parameters &key (initial-state nil) &allow-other-keys)
  (declare (ignorable parameters initial-state))
  (when initial-state
    (push-event object initial-state))
  (locally (declare (special *object*))
    (let ((bordeaux-threads:*default-special-bindings* `((*object* . ,object)
							 ,@bordeaux-threads:*default-special-bindings*)))
      (setf (thread-object object) (bordeaux-threads:make-thread #'(lambda ()
								     (unwind-protect
									  (let ((has-to-exit nil))
									    (loop named state-machine-loop
									       unless (zerop (length (events-queue *object*))) do
										 (set-current-state *object* (pop-event *object*))
										 (multiple-value-bind (f status)
										     (gethash (get-current-state *object*) (handlers-table *object*))
										   (when status
										     (apply f (list *object*))))
									       when has-to-exit do
										 (return-from state-machine-loop)
									       when (eql (get-current-state *object*) :exit) do
										 (setq has-to-exit t)))))
								 :name (symbol-name (gensym "state-machine-")))))))

(defmethod stop ((object state-machine-descriptor))
  (when (running-p object)
    (set-current-state object :exit)
    (loop while (running-p object) do
	 (sleep 1))))

(defmethod push-event ((object state-machine-descriptor) event)
  (bordeaux-threads:with-lock-held ((lock-object object))
    (if (consp (events-queue object))
	(setf (cdr (last (events-queue object))) (cons event nil))
	(setf (events-queue object) (cons event nil)))
    (events-queue object)))

(defmethod pop-event ((object state-machine-descriptor))
  (bordeaux-threads:with-lock-held ((lock-object object))
    (pop (events-queue object))))

;;
;; Macros definition
;;
