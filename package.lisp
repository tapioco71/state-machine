;;;; package.lisp

(defpackage #:state-machine
  (:use #:cl)
  (:export #:state-machine-descriptor
	   #:make-state-machine
	   #:get-name
	   #:set-name
	   #:get-lock
	   #:set-lock
	   #:get-condition-lock
	   #:set-condition-lock
	   #:get-condition
	   #:set-condition
	   #:get-thread
	   #:set-thread
	   #:get-current-state
	   #:set-current-state
	   #:get-events-queue
	   #:set-events-queue
	   #:flush-events-queue
	   #:add-state-handler
	   #:running-p
	   #:start
	   #:stop
	   #:push-event
	   #:pop-event))

